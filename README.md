# ValueAdd Angular meetup


Little play with http

Start server

` node server.js `

Your own http proxy is up and running now!!! Good Job so far!


Examples:

Open network tab in your browser developer tools (F12)

- try to change response code from 200 to 404

- try to change response status text from "OK" to your custom response

- if you're using chrome try to type localhost:8100 without hitting enter. Browser should make request
before you even hit enter

- open http://localhost:8100/test.html - you loaded contents form test.html

- Advanced exercise add image suport to your http proxy


### Setting cookie
edit server.js file
in response line insert
```
HTTP/1.1 200 OK
Set-Cookie: CookieKey=CookieValue

Hello world
```

to check cookie on browser level. Type`document.cookie`
in browser console 

result should contain
`"CookieKey=CookieValue"`

in the result each browser request should contain **Cookie** header
```
GET / HTTP/1.1
Host: localhost:8100
Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
DNT: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,pl;q=0.8,da;q=0.7
Cookie: io=fmBUol10JWf0i9V7AAAA; CookieKey=CookieValue
```

## Useful links
HTTP
https://www.tutorialspoint.com/http/http_requests.htm

Websockets
https://blog.teamtreehouse.com/an-introduction-to-websockets

Selector nesting
https://benfrain.com/css-performance-revisited-selectors-bloat-expensive-styles/

Browser engine
https://en.wikipedia.org/wiki/Browser_engine

Semantics
https://internetingishard.com/html-and-css/semantic-html/

Standards
https://google.github.io/styleguide/htmlcssguide.html

Deep knowledge about JavaScript
https://github.com/getify/You-Dont-Know-JS

Resources from docs:
https://angular.io/resources

Angular blogs:

http://www.angular.love/

https://blog.angularindepth.com/

https://medium.com/@vsavkin/latest

https://blog.thoughtram.io/categories/angular-2/

https://toddmotto.com/

https://netbasal.com/@NetanelBasal

Angular Connect talks:
https://www.youtube.com/channel/UCzrskTiT_ObAk3xBkVxMz5g/videos

Angular subreddit:
https://www.reddit.com/r/Angular2/

State of JavaScript survey:
https://2018.stateofjs.com/

500 pytan z algorytmow i struktur danych z rozmów o prace
https://techiedelight.quora.com/500-Data-structures-and-algorithms-interview-questions-and-their-solutions?share=1

Wersjonowane zadania z rozmów o prace:
https://github.com/yangshun/tech-interview-handbook

Lista ciekawych projektów do napisania:
https://medium.freecodecamp.org/the-secret-to-being-a-top-developer-is-building-things-heres-a-list-of-fun-apps-to-build-aac61ac0736c

Bardziej zaawansowana lista rzeczy do zaprogramowania i zrozumienia samemu:
https://github.com/danistefanovic/build-your-own-x

Frontend Checklist:
https://github.com/thedaviddias/Front-End-Checklist
