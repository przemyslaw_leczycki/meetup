'use strict';

const net = require('net');
const fs = require('fs');

let server = net.createServer((socket) => {
	let clientName = `${socket.remoteAddress}`;
 	console.log(`${clientName} connected.`);

  	socket.on('data', (data) => {
  		console.log(data.toString());
	  	const requestedUrl = data.toString().split('\n')[0].split(' ')[1];
  		let filepath = '.' + requestedUrl;
	  	if (requestedUrl === '/') {
	  		filepath = './index.html';
	  	}
	
	  	fs.readFile(filepath, function (err, data) {
	  		let response = '';
	  		if (err && err.errno === -2) {
	  			response = 'HTTP/1.1 404 Not Found\n\n<h1>Not found 404</h1>';
	  		} else {
	  			response = `HTTP/1.1 200 OK\n\n${data}`;
	  		}
	  		console.log(response);
	  		socket.write(response);
	  		socket.destroy();
		});
	});
});
  
server.listen(8100, '127.0.0.1');
console.log('Server started at localhost:8100');
